
import React from "react";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Note from "./components/Note";
import notes from "./note.js";
import Form from "./components/Form";


function App() {

  const [notes, setNote] = React.useState([])

  function addNote (note) {
    setNote(prevNotes => {
      return [...prevNotes, note]
    })
  }
  function deleteItems (id) {
    setNote(prevNotes => {
      return prevNotes.filter((newItem, index) => {
        return index !== id;
      });
    });
  };

  return (
     <div>
     <Header />
     <Form onAdd={addNote}/>
     {notes.map((newItem, index) => {
      return (
        <Note
        key={index}
        id={index}
        title={newItem.title}
        content={newItem.content}
        onDelete={deleteItems}
        />)
     })}
     <Footer />
     </div>
  );
}

export default App;
