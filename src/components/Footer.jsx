import React from "react";


function Footer () {

const date = new Date;
const correctYear = date.getFullYear();

  return (
    <footer>
    <p>Copyright  &copy; {correctYear}</p>
    </footer>
  );
};


export default Footer;
