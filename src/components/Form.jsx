import React, {useState} from "react";
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import Zoom from '@mui/material/Zoom';


function Form (props) {

  const [note, setNote] = useState({title: "",content: ""});
  const [isExpanded,setExpand] = useState(false);

  function heandleTitle () {
    setExpand(true);
  }


  function heandleChange (event) {

    const {name, value} = event.target;
    setNote(prevValue => {
      return {
        ...prevValue,
        [name]: value
      };
    });
  };
  function submitButton (event) {
    props.onAdd(note)
    setNote({
      title: "",
      content: ""
    });
    event.preventDefault();

  };


  return (
  <div>
   <form className="form create-note">
   {isExpanded ?
   <input
   onChange={heandleChange}
   name="title"
   placeholder="Title"
   value={note.title}/> : null}
   <textarea
   onClick={heandleTitle}
   onChange={heandleChange}
   name="content"
   placeholder="Take a note..."
   value={note.content}
   rows={isExpanded ? 3 : 1}
   />
<Zoom in={isExpanded ? true : false}>
   <Button
   onClick={submitButton}
   class="btn btn-primary">
   <AddIcon />
   </Button>
</Zoom>
   </form>
  </div>
  )
}

export default Form;
